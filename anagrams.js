'use strict';
/**
 * Anagrams
 * @module anagrams
 *
 * Create a function that finds two words in a text that are anagrams of another
 * two words in that text.
 *
 * Ex: 'Happy eaters always heat their yappers.' => 'happy eaters and heat yappers'
 *
 * - Treat all letters as lowercase.
 * - Ignore any words less than 4 characters long.
 * - Treat all non-alpha-numeric characters as whitespace.
 * - So "Surely. And they're completely right!" becomes four words: "surely
 * 			 they completely right"
 * - Neither of the words in the first pair can be repeated in the second pair.
 */

// Very useful module
// https://lodash.com/docs
const _ = require('lodash');

/**
 * Searches for bigram anagrams, excluding chosen bigram from indexes
 *
 * @param {array} tokens - text as array of words
 * @param {number} indexFirstToken - one word of chosen bigram
 * @param {number} indexSecondToken - another word of chosen bigram
 */
const search = (tokens, indexFirstToken, indexSecondToken) => {
  // Add the words together and sort the chars to make comparision quick
  let sortedAnagram = tokens[indexFirstToken]
    .split('')
    .concat(tokens[indexSecondToken].split(''))
    .sort()
    .join('');
  //console.log('sortedAnagram', sortedAnagram);
  for (let i = 0; i < tokens.length; ++i) {
    // if the token isn't one of the words already used
    if (i !== indexFirstToken && i !== indexSecondToken) {
      for (let k = i + 1; k < tokens.length; ++k) {
        // if the token isn't one of the words already used
        // and their lengths add up to the sorted anagram
          if(k !== indexFirstToken && k !== indexSecondToken &&
          sortedAnagram.length === tokens[i].length + tokens[k].length) { // checking length saves so much time
          // Add the words together and sort the chars to make comparision quick
          let testSortedAnagram = tokens[i]
            .split('')
            .concat(tokens[k].split(''))
            .sort()
            .join('');
          //console.log('testSortedAnagram', testSortedAnagram);
          if (testSortedAnagram === sortedAnagram) {
            // Return early
            return [tokens[i], tokens[k]];
          }
        }
      }
    }
  }
  // No anagrams
  return null;
};

/**
 * Finds two random bigrams and their bigram analog
 *
 * @param {sting} text - text to find anagrams in
 *
 * @return {array[array] or null} e.g [ [ 'amet', 'ultricies' ], [ 'ultrices', 'etiam' ] ]
 */
const anagrams = (text, f, s) => {
  // Tokenize the text
  // Could be done more efficiently using a buffer, but this will do
  let tokens = _.uniq(text
    .toLocaleLowerCase()
    .replace(/[^a-z0-9]/g, ' ')
    .split(' ')
    .filter(word => {
      return word.length > 3;
    }));
  //console.log(tokens.sort());
  // For each combinations of tokens, see if there are any anagrams
  for (let i = 0; i < tokens.length; ++i) {
    for (let k = i + 1; k < tokens.length; ++k) {
      let anagram = search(tokens, i, k);
      if (anagram !== null) {
        return [
          [tokens[i], tokens[k]],
          anagram
        ];
      }
    }
  }
  return null;
};

//export anagrams;
module.exports = anagrams;
