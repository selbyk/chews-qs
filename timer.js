'use strict';
/**
 * Timer
 * @module timer
 *
 * Just a simple timer to measure performace
 */
class Timer {
  constructor(){
    this.startTime = Date.now();
    this.times = {};
  }
  start(tag) {
    let d = Date.now();
    this.times[tag] = d;
    console.log('[', tag,'] Timer started at ', this.times[tag]);
  }
  end(tag){
    let d = Date.now();
    console.log('[', tag,'] Timer clocked at ', d-this.times[tag], 'ms / ', (d - this.times[tag])/1000, 's');
    this.times[tag] = d;
  }
}

module.exports = Timer;
