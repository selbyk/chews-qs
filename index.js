'use strict';
/**
 * chews-qs
 * @module chews-qs
 *
 * Interview questions
 */
const fs = require('fs');
//import { predict } from './stocks';
const predict = require('./stocks');
//import findAnagrams from './anagrams';
const findAnagrams = require('./anagrams');
const Timer = require('./timer');
const timer = new Timer();

/**
 * Anagrams
 *
 * Create a function that finds two words in a text that are anagrams of another
 * two words in that text.
 *
 * Ex: 'Happy eaters always heat their yappers.' => 'happy eaters and heat yappers'
 *
 * - Treat all letters as lowercase.
 * - Ignore any words less than 4 characters long.
 * - Treat all non-alpha-numeric characters as whitespace.
 * - So "Surely. And they're completely right!" becomes four words: "surely
 * 			 they completely right"
 * - Neither of the words in the first pair can be repeated in the second pair.
 */
fs.readFile('./long-text.txt', 'utf8', function(err, longText) {
  let anagramTests = [
    'Happy eaters always heat their yappers.',
    'Surely. And they\'re completely right!',
    longText
  ];

  anagramTests.forEach(text => {
    if (text.length > 64) {
      console.log('Using: ', text.slice(0, 64), '...');
    } else {
      console.log('Using: ', text);
    }
    timer.start('anagrams');
    console.log('Finding anagrams...');
    let anagrams = findAnagrams(text);
    timer.end('anagrams');
    console.log(anagrams);
    console.log();
  });
});

/**
 * Stock Prices
 *
 * Given a list of integers representing stock prices by day, return the
 * optimum buy and sell points to maximize profit. (You can only make one buy
 * and one sell.) Your function should take a list as input and return the two
 * indexes of the buy and sell points.
 */
let stockTests = [
  [],
  [1],
  [1, 2, 3, 4, 5],
  [1, 2, 4, 5, 1, 2, 3, 4],
  [45, 46, 43, 45, 44, 41, 42, 41]
];

stockTests.forEach(prices => {
  console.log('Using: ', prices);
  timer.start('stock');
  console.log('Predicting the market...');
  let prediction = predict.optimum(prices);
  timer.end('stock');
  console.log(prediction);
  console.log();
});
