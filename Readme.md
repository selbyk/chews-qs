Showterm: http://showterm.io/f66961a86dbee0e34adbb

1. Stock Prices

Given a list of integers representing stock prices by day, return the optimum buy and sell points to maximize profit. (You can only make one buy and one sell.) Your function should take a list as input and return the two indexes of the buy and sell points.

2. Anagrams

Create a function that finds two words in a text that are anagrams of another two words in that text. For example:

Happy eaters always heat their yappers.
Would yield: happy eaters and heat yappers

The function should also work on long blocks of text.

Rules:
- Treat all letters as lowercase.
- Ignore any words less than 4 characters long.
- Treat all non-alpha-numeric characters as whitespace.
- So "Surely. And they're completely right!" becomes four words: "surely  they completely right"
- Neither of the words in the first pair can be repeated in the second pair.

Please let me know if you have any questions or clarifications!
