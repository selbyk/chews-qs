'use strict';
/**
 * Stocks
 * @module stocks
 *
 * Given a list of integers representing stock prices by day, return the
 * optimum buy and sell points to maximize profit. (You can only make one buy
 * and one sell.) Your function should take a list as input and return the two
 * indexes of the buy and sell points.
 */

// Very useful module
// https://lodash.com/docs
const _ = require('lodash');

// Prediction algorithms
const predict = {
  /**
   * Find optiminal buy and sell index
   *
   * @param {array} prices - stock price by day
   *
   * @return {object} buyIndex and sellIndex
   */
  optimum(prices) {
      let buy = 0,
        sell = 0;
      try {
        //console.log(prices);
        //console.log(_.isArray(prices));
        if (_.isArray(prices) === false) {
          throw new TypeError('Prices must be an array!');
        }
        if (_.isEmpty(prices)) {
          throw new Error('prices array empty');
          console.log(err.message);
        }
        let b = 0,
          s = prices.length - 1,
          count = 1;
        sell = s;
        while (b !== s) {
          if (count++ % 2) {
            if (prices[--s] > prices[sell]) {
              sell = s;
            }
          } else {
            if (prices[++b] < prices[buy]) {
              buy = b;
            }
          }
        }
      } catch (e) {
        buy = 0;
        sell = 0;
        if (e instanceof TypeError) {
          console.log('TypeError');
        } else if (e.message === 'prices array empty') {
          buy = null;
          sell = null;
        }
      }
      return {
        buyIndex: buy,
        sellIndex: sell
      }
    },
    // MACD: (prices) => {},
    // MA: (prices) => {},
    // ETC: (prices) => {}
};

//export predict;
module.exports = predict;
